package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Category;
import edu.ceis.ssiserviceceis.repositories.CategoryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Set;

import static org.mockito.Mockito.*;

public class CategoryServiceImplTest {
    @Mock
    CategoryRepository categoryRepository;
    @InjectMocks
    CategoryServiceImpl categoryServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        final ArrayList<Category> categories = new ArrayList<>();
        categories.add(new Category());
        categories.add(new Category());
        when(categoryRepository.findAll()).thenReturn(categories);
    }

    @Test
    public void testGetCategories() throws Exception {
        Set<Category> result = categoryServiceImpl.findAll();
        Assert.assertEquals(2, result.size());
        verify(categoryRepository, times(1)).findAll();
    }
}