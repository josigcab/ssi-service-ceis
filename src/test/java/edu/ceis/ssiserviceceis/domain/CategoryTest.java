package edu.ceis.ssiserviceceis.domain;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class CategoryTest {

    private static final String TEST_CAT_NAME = "TEST_CAT_NAME";

    @Test
    public void getName() {
        Category category = new Category();
        category.setName(TEST_CAT_NAME);

        assertNotNull(category.getName());
        Assert.assertEquals(TEST_CAT_NAME, category.getName());
    }
}