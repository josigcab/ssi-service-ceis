package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.domain.Category;
import edu.ceis.ssiserviceceis.services.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class CategoryControllerTest {
    private static final String CATEGORIES_VIEW = "categories";
    @Mock
    CategoryService categoryService;
    @Mock
    private Model model;
    @InjectMocks
    CategoryController categoryController;

    private HashSet<Category> categorySet;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        categorySet = new HashSet<>();
        categorySet.add(new Category());
        when(categoryService.findAll()).thenReturn(categorySet);

    }

    @Test
    public void testGetCategories() throws Exception {
        Set<Category> catSetClassInstance = new HashSet<>();
        ArgumentCaptor<Set<Category>> argumentCaptor = ArgumentCaptor.forClass(catSetClassInstance.getClass());

        final String code = "";
        String result = categoryController.getCategories(code, model);
        assertEquals(CATEGORIES_VIEW, result);
        verify(categoryService).findAll();
        verify(categoryService, times(0)).findByCode(code);
        verify(model, times(1)).addAttribute("categories", categorySet);
        verify(model, times(1)).addAttribute(eq("categories"), argumentCaptor.capture());
        Set<Category> capturedCategories = argumentCaptor.getValue();
        assertEquals(capturedCategories.size(), 1);

    }


}
