/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.integration;

import edu.ceis.ssiserviceceis.domain.Category;
import edu.ceis.ssiserviceceis.repositories.CategoryRepository;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;
import java.util.Set;

@DataJpaTest
@RunWith(SpringRunner.class)
public class CategoryRepositoryIT {
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    @Ignore
    public void tesFindAllCategoriesIT() {
        Set<Category> categories = new HashSet<>();
        final Iterable<Category> all = categoryRepository.findAll();
        all.iterator().forEachRemaining(categories::add);
        Assert.assertEquals(1, categories.size());
    }
}
