/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Category;
import edu.ceis.ssiserviceceis.repositories.CategoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category> implements CategoryService {

    private CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Category> findByCode(String code) {
        return repository.findByCode(code).get();
    }

    @Override
    protected CrudRepository<Category, Long> getRepository() {
        return repository;
    }
}
