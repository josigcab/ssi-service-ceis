/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Category;

import java.util.List;

public interface CategoryService extends GenericService<Category> {

    List<Category> findByCode(String code);

}
