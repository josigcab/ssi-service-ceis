/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Employee;

import java.io.InputStream;

public interface EmployeeService extends GenericService<Employee> {
    void saveImage(Long id, InputStream inputStream);
}
