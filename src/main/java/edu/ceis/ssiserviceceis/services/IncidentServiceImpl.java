/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Incident;
import edu.ceis.ssiserviceceis.repositories.IncidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncidentServiceImpl extends GenericServiceImpl<Incident> implements IncidentService {

    private IncidentRepository repository;

    public IncidentServiceImpl(IncidentRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Incident> findByCode(String code) {
        return repository.findByCode(code).get();
    }

    @Override
    protected CrudRepository<Incident, Long> getRepository() {
        return repository;
    }
}
