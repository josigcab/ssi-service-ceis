/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {
}
