/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.BuyOrder;
import edu.ceis.ssiserviceceis.repositories.BuyOrderRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuyOrderServiceImpl extends GenericServiceImpl<BuyOrder> implements BuyOrderService {

    private BuyOrderRepository repository;

    public BuyOrderServiceImpl(BuyOrderRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<BuyOrder> findByCode(String code) {
        return repository.findByCode(code).get();
    }

    @Override
    protected CrudRepository<BuyOrder, Long> getRepository() {
        return repository;
    }
}
