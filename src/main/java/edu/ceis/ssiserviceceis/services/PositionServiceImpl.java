/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Position;
import edu.ceis.ssiserviceceis.repositories.PositionRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class PositionServiceImpl extends GenericServiceImpl<Position> implements PositionService {

    private PositionRepository repository;

    public PositionServiceImpl(PositionRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Position, Long> getRepository() {
        return repository;
    }
}
