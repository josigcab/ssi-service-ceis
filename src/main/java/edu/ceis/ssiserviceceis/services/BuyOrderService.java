/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.BuyOrder;

import java.util.List;

public interface BuyOrderService extends GenericService<BuyOrder> {
    List<BuyOrder> findByCode(String code);
}
