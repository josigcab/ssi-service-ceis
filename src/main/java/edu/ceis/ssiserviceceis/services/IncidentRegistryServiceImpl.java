/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.IncidentRegistry;
import edu.ceis.ssiserviceceis.repositories.IncidentRegistryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncidentRegistryServiceImpl extends GenericServiceImpl<IncidentRegistry> implements IncidentRegistryService {

    private IncidentRegistryRepository repository;

    public IncidentRegistryServiceImpl(IncidentRegistryRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<IncidentRegistry> findByCode(String code) {
        return repository.findByCode(code).get();
    }

    @Override
    protected CrudRepository<IncidentRegistry, Long> getRepository() {
        return repository;
    }
}
