/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.ModelBase;
import edu.ceis.ssiserviceceis.exceptions.NotFoundException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public abstract class GenericServiceImpl<T extends ModelBase> implements GenericService<T> {

    @Override
    public List<T> findAll() {
        List<T> elements = new ArrayList<>();
        getRepository().findAll().forEach(elements::add);
        return elements;
    }

    @Override
    public T findById(Long id) {
        final Optional<T> optional = getRepository().findById(id);
        if (!optional.isPresent()) {
            String typeName = (((ParameterizedType) getClass()
                    .getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName();
            typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
            throw new NotFoundException(String.format("%s Not found with id %s", typeName, id));
        } else {
            return optional.get();
        }
    }

    @Override
    public T save(T model) {
        return getRepository().save(model);
    }

    @Override
    public void deleteById(Long id) {
        getRepository().deleteById(id);
    }

    protected abstract CrudRepository<T, Long> getRepository();

}
