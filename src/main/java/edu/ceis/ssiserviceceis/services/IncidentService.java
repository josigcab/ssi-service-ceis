/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.Incident;

import java.util.List;

public interface IncidentService extends GenericService<Incident> {

    List<Incident> findByCode(String code);

}
