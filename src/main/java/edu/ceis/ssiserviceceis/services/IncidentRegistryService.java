/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.services;

import edu.ceis.ssiserviceceis.domain.IncidentRegistry;

import java.util.List;

public interface IncidentRegistryService extends GenericService<IncidentRegistry> {

    List<IncidentRegistry> findByCode(String code);

}
