/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class IncidentRegistry extends ModelBase {
    @Column
    private String name;
    @Column
    private String code;
    @OneToOne(optional = false)
    private Employee employee;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
