/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BuyOrder extends ModelBase {
    @Column
    private String provider;
    @Column
    private String code;

    @OneToMany(mappedBy = "buyOrder", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<Item> items = new ArrayList<>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProvider() {

        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
