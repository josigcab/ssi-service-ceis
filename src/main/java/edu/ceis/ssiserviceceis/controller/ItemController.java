/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controller;

import edu.ceis.ssiserviceceis.domain.Item;
import edu.ceis.ssiserviceceis.repositories.ItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping(value = "/execute")
public class ItemController {
    private final EntityManager em;
    private ItemRepository itemRepository;

    public ItemController(EntityManager em, ItemRepository itemRepository) {
        this.em = em;
        this.itemRepository = itemRepository;
    }

    @RequestMapping
    @Transactional
    public ResponseEntity<List<Item>> execute() {
        /*Query insertFrom = em.createNamedQuery("insertFrom");
        insertFrom.executeUpdate();*/
        itemRepository.modifyingQueryInsertItem();
        Query query = em.createNamedQuery("select");
        List<Item> parts = query.getResultList();
        return new ResponseEntity<>(parts, HttpStatus.OK);
    }
}
