/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package edu.ceis.ssiserviceceis.dao;

import edu.ceis.ssiserviceceis.domain.IncidentRegistry;

public class IncidentRegistryDto extends DtoBase<IncidentRegistry> {

    private String name;
    private String code;
    private Long id;

    public IncidentRegistry toDomain() {
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setCode(getCode());
        incidentRegistry.setId(getId());
        incidentRegistry.setName(getName());
        return incidentRegistry;
    }

    @Override
    public IncidentRegistryDto toDto(IncidentRegistry incidentRegistry) {
        IncidentRegistryDto incidentRegistryDto = new IncidentRegistryDto();
        incidentRegistryDto.setId(incidentRegistry.getId());
        incidentRegistryDto.setName(incidentRegistry.getName());
        return incidentRegistryDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

}
