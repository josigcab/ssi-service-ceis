/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package edu.ceis.ssiserviceceis.dao;


import edu.ceis.ssiserviceceis.domain.BuyOrder;
import edu.ceis.ssiserviceceis.domain.Item;

import java.util.List;

public class BuyOrderDto extends DtoBase<BuyOrder> {

    private String name;
    private String code;
    private String provider;
    private Long id;
    private List<Item> items;

    public BuyOrderDto(BuyOrder buyOrder) {
        setId(buyOrder.getId());
        setVersion(buyOrder.getVersion());
        setCreatedOn(buyOrder.getCreatedOn());
        setUpdatedOn(buyOrder.getUpdatedOn());
    }

    public BuyOrderDto() {
        super();
    }

    @Override
    public BuyOrderDto toDto(BuyOrder buyOrder) {
        BuyOrderDto buyOrderDto = new BuyOrderDto();
        buyOrderDto.setId(buyOrder.getId());
        buyOrderDto.setCode(buyOrder.getCode());
        buyOrderDto.setProvider(buyOrder.getProvider());
        buyOrderDto.setItems(buyOrder.getItems());
        return buyOrderDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public BuyOrder toBuyOrder() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setId(getId());
        buyOrder.setVersion(getVersion());
        buyOrder.setCreatedOn(getCreatedOn());
        buyOrder.setUpdatedOn(getUpdatedOn());
        return buyOrder;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
