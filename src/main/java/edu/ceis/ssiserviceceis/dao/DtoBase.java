/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.dao;

import edu.ceis.ssiserviceceis.domain.ModelBase;

import java.util.Date;

public abstract class DtoBase<E extends ModelBase> {

    private Long id;
    private Date createdOn;
    private Date updatedOn;
    private long version;

    public abstract DtoBase toDto(E element);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
