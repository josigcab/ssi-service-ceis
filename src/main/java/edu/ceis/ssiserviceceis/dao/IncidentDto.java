/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package edu.ceis.ssiserviceceis.dao;


import edu.ceis.ssiserviceceis.domain.Incident;
import org.apache.tomcat.util.codec.binary.Base64;

public class IncidentDto extends DtoBase<Incident> {

    private String name;
    private String code;
    private Long id;
    private String description;

    public Incident toDomain() {
        Incident incident = new Incident();
        incident.setCode(getCode());
        incident.setId(getId());
        incident.setName(getName());
        incident.setDescription(getDescription());
        return incident;
    }

    @Override
    public IncidentDto toDto(Incident incident) {
        IncidentDto incidentDto = new IncidentDto();
        incidentDto.setDescription(incident.getName());
        incidentDto.setId(incident.getId());
        incidentDto.setName(incident.getName());
        return incidentDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
