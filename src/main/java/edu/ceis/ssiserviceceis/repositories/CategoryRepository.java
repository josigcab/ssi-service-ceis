package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<List<Category>> findByCode(String code);
}
