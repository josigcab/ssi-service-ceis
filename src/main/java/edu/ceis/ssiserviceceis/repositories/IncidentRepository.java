package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Incident;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
    Optional<List<Incident>> findByCode(String code);
}
