package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ItemRepository extends JpaRepository<Item, Long> {
    @Modifying
    @Query("insert INTO Item (id, name, code, createdOn, version) select p.id*10, p.name, p.code, p.createdOn, p" +
            ".version from Part p")
    int modifyingQueryInsertItem();

}
