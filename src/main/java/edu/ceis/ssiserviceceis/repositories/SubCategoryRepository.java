package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Category;
import edu.ceis.ssiserviceceis.domain.SubCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
    Optional<List<Category>> findByCode(String code);
}
