package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Incident;
import edu.ceis.ssiserviceceis.domain.IncidentRegistry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
    Optional<List<IncidentRegistry>> findByCode(String code);
}
