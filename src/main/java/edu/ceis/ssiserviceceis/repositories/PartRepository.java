package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Part;
import org.springframework.data.repository.CrudRepository;

public interface PartRepository extends CrudRepository<Part, Long> {
}
