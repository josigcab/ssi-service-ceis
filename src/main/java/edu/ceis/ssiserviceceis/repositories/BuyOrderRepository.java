package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.BuyOrder;
import edu.ceis.ssiserviceceis.domain.Incident;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {
    Optional<List<BuyOrder>> findByCode(String code);
}
