/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.dao.BuyOrderDto;
import edu.ceis.ssiserviceceis.domain.BuyOrder;
import edu.ceis.ssiserviceceis.services.BuyOrderService;
import edu.ceis.ssiserviceceis.services.GenericService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/buyOrders")
@Controller
@Produces(MediaType.APPLICATION_JSON)
public class BuyOrderController extends GenericController<BuyOrder, BuyOrderDto> {
    private BuyOrderService service;

    public BuyOrderController(BuyOrderService service) {
        this.service = service;
    }

    @GET
    public Response getBuyOrders() {
        return super.getElements();
    }

    @GET
    @Path("/{id}")
    public BuyOrderDto getBuyOrderById(@PathParam("id") Long id) {
        BuyOrder buyOrder = service.findById(id);
        return new BuyOrderDto(buyOrder);
    }

    @OPTIONS
    public Response preflight() {
        return super.preflight();
    }
    @POST
    public BuyOrderDto addBuyOrder(BuyOrderDto buyOrderDto) {
        BuyOrder buyOrder = service.save(buyOrderDto.toBuyOrder());
        return new BuyOrderDto(buyOrder);
    }

    @PUT
    public BuyOrderDto updateBuyOrder(BuyOrderDto buyOrderDto) {
        BuyOrder buyOrder = service.save(buyOrderDto.toBuyOrder());
        return new BuyOrderDto(buyOrder);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteBuyOrder(@PathParam("id") long id) {
        return super.deleteElement(id);
    }

    @Override
    protected GenericService getService() {
        return service;
    }
}
