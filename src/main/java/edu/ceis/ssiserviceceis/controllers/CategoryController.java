/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.domain.Category;
import edu.ceis.ssiserviceceis.services.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryController {

    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GET
    public Response getCategories(@QueryParam("code") String code) {
        List<Category> categories = categoryService.findByCode(code);
        Response.ResponseBuilder responseBuilder = Response.ok(categories);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getCategoriesById(@PathParam("id") @NotNull Long id) {
        Category category = categoryService.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(category);
        return responseBuilder.build();
    }

}
