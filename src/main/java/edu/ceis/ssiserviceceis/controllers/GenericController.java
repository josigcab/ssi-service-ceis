package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.dao.DtoBase;
import edu.ceis.ssiserviceceis.domain.ModelBase;
import edu.ceis.ssiserviceceis.services.GenericService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.Response;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class GenericController<E, D extends DtoBase> {

    @Autowired
    protected ModelMapper modelMapper;

    protected abstract GenericService getService();

    protected Response preflight() {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    public Response getElements() {
        List<DtoBase> items = new ArrayList<>();
        final D itemDto = getInstanceOfD();
        final List elements = getService().findAll();
        elements.forEach(item -> items.add(itemDto.toDto((ModelBase) item)));
        return Response.ok(items).build();
    }

    D getInstanceOfD() {
        ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
        Class<D> type = (Class<D>) superClass.getActualTypeArguments()[1];
        try {
            return type.newInstance();
        } catch (Exception e) {
            // Oops, no default constructor
            throw new RuntimeException(e);
        }
    }

    protected Response deleteElement(Long id) {
        getService().deleteById(id);
        return Response.ok().build();
    }

    public <T> List<T> convertListToDtoList(List<?> elements, Class<T> classToConvert) {
        return elements.stream().map(element -> modelMapper.map(element, classToConvert)).collect(Collectors.toList());
    }

}
