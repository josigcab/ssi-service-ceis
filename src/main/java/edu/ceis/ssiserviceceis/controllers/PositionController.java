/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.domain.Position;
import edu.ceis.ssiserviceceis.services.PositionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/positions")
public class PositionController {
    private PositionService service;

    public PositionController(PositionService service) {
        this.service = service;
    }

    @GetMapping
    public List<Position> getPositions() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public Position getPositionsById(@PathVariable("id") @NotNull Long id) {
        return service.findById(id);
    }
}    
