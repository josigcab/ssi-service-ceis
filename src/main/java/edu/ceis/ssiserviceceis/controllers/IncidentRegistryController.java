/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.dao.IncidentRegistryDto;
import edu.ceis.ssiserviceceis.domain.IncidentRegistry;
import edu.ceis.ssiserviceceis.services.GenericService;
import edu.ceis.ssiserviceceis.services.IncidentRegistryService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Controller
@Path("/incidentRegisters")
@Produces(MediaType.APPLICATION_JSON)
public class IncidentRegistryController extends GenericController<IncidentRegistry, IncidentRegistryDto> {
    private IncidentRegistryService service;

    public IncidentRegistryController(IncidentRegistryService service) {
        this.service = service;
    }

    @GET
    public Response getIncidentRegisters() {
        return super.getElements();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentRegistryById(@PathParam("id") long id) {
        IncidentRegistry incidentRegistry = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentRegistryDto().toDto(incidentRegistry));
        return responseBuilder.build();
    }

    @POST
    public Response saveIncidentRegistry(IncidentRegistryDto incidentRegistry) {
        IncidentRegistry model = incidentRegistry.toDomain();
        IncidentRegistry incidentRegistryPersisted = service.save(model);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentRegistryDto().toDto(incidentRegistryPersisted));
        return responseBuilder.build();
    }

    @PUT
    public Response updateIncidentRegistry(IncidentRegistry incidentRegistry) {
        IncidentRegistry incidentRegistryPersisted = service.save(incidentRegistry);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentRegistryDto().toDto(incidentRegistryPersisted));
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncidentRegistry(@PathParam("id") Long id) {
        return super.deleteElement(id);
    }

    @OPTIONS
    public Response prefligth() {
        return super.preflight();
    }

    @Override
    protected GenericService getService() {
        return service;
    }
}
