/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.dao.ItemDto;
import edu.ceis.ssiserviceceis.domain.Item;
import edu.ceis.ssiserviceceis.services.GenericService;
import edu.ceis.ssiserviceceis.services.ItemService;
import edu.ceis.ssiserviceceis.services.SubCategoryService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Controller
@Path("/items")
@Produces(MediaType.APPLICATION_JSON)
public class ItemController extends GenericController<Item, ItemDto> {
    private ItemService service;
    private SubCategoryService subCategoryService;

    public ItemController(ItemService service, SubCategoryService subCategoryService) {
        this.service = service;
        this.subCategoryService = subCategoryService;
    }

    @GET
    public Response getItems() {
        return super.getElements();
    }

    @GET
    @Path("/{id}")
    public Response getItemById(@PathParam("id") long id) {
        Item item = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new ItemDto().toDto(item));
        return responseBuilder.build();
    }

    @POST
    public Response saveItem(ItemDto item) {
        Item model = item.toDomain();
        model.setSubCategory(subCategoryService.findById(item.getSubCategoryId()));
        Item itemPersisted = service.save(model);
        Response.ResponseBuilder responseBuilder = Response.ok(new ItemDto().toDto(itemPersisted));
        return responseBuilder.build();
    }

    @PUT
    public Response updateItem(Item item) {
        Item itemPersisted = service.save(item);
        Response.ResponseBuilder responseBuilder = Response.ok(new ItemDto().toDto(itemPersisted));
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") Long id) {
        return super.deleteElement(id);
    }

    @OPTIONS
    public Response prefligth() {
        return super.preflight();
    }

    @Path("/{id}/image")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFile(@PathParam("id") String id,
                               @FormDataParam("file") InputStream file,
                               @FormDataParam("file") FormDataContentDisposition fileDisposition) {
        service.saveImage(Long.valueOf(id), file);
        return Response.ok("Data uploaded successfully !!").build();
    }

    @Override
    protected GenericService getService() {
        return service;
    }
}
