/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.dao.EmployeeDto;
import edu.ceis.ssiserviceceis.domain.Employee;
import edu.ceis.ssiserviceceis.services.EmployeeService;
import edu.ceis.ssiserviceceis.services.GenericService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Path("/employees")
@Controller
@Produces(MediaType.APPLICATION_JSON)
public class EmployeeController extends GenericController<Employee, EmployeeDto> {
    private EmployeeService service;

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @GET
    public Response getEmployees() {
        return super.getElements();
    }

    @GET
    @Path("/{id}")
    public EmployeeDto getEmployeeById(@PathParam("id") Long id) {
        Employee employee = service.findById(id);
        return new EmployeeDto(employee);
    }

    @OPTIONS
    public Response preflight() {
        return super.preflight();
    }
    @POST
    public EmployeeDto addEmployee(EmployeeDto employeeDto) {
        Employee employee = service.save(employeeDto.toEmployee());
        return new EmployeeDto(employee);
    }

    @PUT
    public EmployeeDto updateEmployee(EmployeeDto employeeDto) {
        Employee employee = service.save(employeeDto.toEmployee());
        return new EmployeeDto(employee);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteEmployee(@PathParam("id") long id) {
        return super.deleteElement(id);
    }

    @Path("/{id}/image")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFile(@PathParam("id") String id,
                               @FormDataParam("file") InputStream file,
                               @FormDataParam("file") FormDataContentDisposition fileDisposition) {
        service.saveImage(Long.valueOf(id), file);
        return Response.ok("Data uploaded successfully !!").build();
    }

    @Override
    protected GenericService getService() {
        return service;
    }
    /*
    https://www.getpostman.com/collections/cb9764af6c5d5bcaa0c9
    */
}
