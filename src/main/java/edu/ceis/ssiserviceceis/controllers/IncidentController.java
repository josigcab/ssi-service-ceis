/**
 * @author: Jose Cabrera
 */

package edu.ceis.ssiserviceceis.controllers;

import edu.ceis.ssiserviceceis.dao.IncidentDto;
import edu.ceis.ssiserviceceis.domain.Incident;
import edu.ceis.ssiserviceceis.services.GenericService;
import edu.ceis.ssiserviceceis.services.IncidentService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Controller
@Path("/incidents")
@Produces(MediaType.APPLICATION_JSON)
public class IncidentController extends GenericController<Incident, IncidentDto> {
    private IncidentService service;

    public IncidentController(IncidentService service) {
        this.service = service;
    }

    @GET
    public Response getIncidents() {
        return super.getElements();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentById(@PathParam("id") long id) {
        Incident incident = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentDto().toDto(incident));
        return responseBuilder.build();
    }

    @POST
    public Response saveIncident(IncidentDto incident) {
        Incident model = incident.toDomain();
        Incident incidentPersisted = service.save(model);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentDto().toDto(incidentPersisted));
        return responseBuilder.build();
    }

    @PUT
    public Response updateIncident(Incident incident) {
        Incident incidentPersisted = service.save(incident);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentDto().toDto(incidentPersisted));
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncident(@PathParam("id") Long id) {
        return super.deleteElement(id);
    }

    @OPTIONS
    public Response prefligth() {
        return super.preflight();
    }

    @Override
    protected GenericService getService() {
        return service;
    }
}
